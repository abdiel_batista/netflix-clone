<h1 align="center">
<br>
    <img 
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Netflix_2015_logo.svg/2560px-Netflix_2015_logo.svg.png" 
        alt="Netflix" 
        width="120"
    />
<br>
NetFlix Clone
</h1>


<p align="center">
    Ultilizando ReactJS, estou desenvolvendo um clone da página inicial do NetFlix.<br />
    Projeto excente, feito durante a Live do <strong><a href="https://github.com/bonieky">Bonieky Lacerda</a></strong> da <strong><a href="https://www.linkedin.com/company/b7web/" target="_blank">(B7Web)</a></strong>.
    <br />
    Neste projeto foi consumida uma API externa do site <strong><a href="https://www.themoviedb.org/" target="_blank">Themoviedb.com</a></strong> para exibição dos filmes e séries, utilizando-se das manipulações de estados com o useState do React.
</p>

<div align="center">
    <img src="https://github.com/ivisconfessor/netflixclone/blob/master/github/NetFlixCloneWeb.gif?raw=true"
    alt="demo-web" height="425" />
    <img src="./github/NetFlixCloneMobile.gif"
    alt="demo-mobile" height="425" />
</div>

<hr />

<div align="center">

## 🚀 Tecnologias usadas:

✔️ ReactJS

✔️ JSX

✔️ CSS

✔️ JavaScript

✔️ API TMDB

</div>

<hr />
<!--

<div align="center">
    Feito com <span role="img" aria-label="coração">❤️</span> por <strong><a href="https://github.com/ivisconfessor">Ivís Confessor</a></strong> 
    <br/>com a orientação do <strong><a href="https://github.com/bonieky">
    Bonieky Larceda</a></strong> durante a Live <span role="img" aria-label="fogo">🔥</span> Clone do NETFLIX em REACTJS para Iniciantes <strong> em 24/08/2020</strong><br/><br/>
    Direitos de imagem para Netflix<br/>
    Dados pegos do site Themoviedb.org
</div>

-->
